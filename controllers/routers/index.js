const router = require('express').Router();
// require('../db/form');
// const bodyparser = require('body-parser');

// // router.use(database);
// router.use(bodyparser.urlencoded({extended: true}));
router.get('/' , (req , res)=>{
    res.render('index.ejs' , {
        title:'Home Page'
    });
});
router.get('/contact' , (req , res)=>{
   res.render('contact.ejs',{
       title:'Contact me'
   });
});
router.get('/about-us' , (req , res)=>{
    res.render('about.ejs',{
        title:'About Us'
    })
})






module.exports = router;
